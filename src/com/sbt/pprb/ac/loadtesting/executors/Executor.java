package com.sbt.pprb.ac.loadtesting.executors;

import com.sbt.pprb.ac.loadtesting.Result;
import com.sbt.pprb.ac.loadtesting.parsers.Information;

/**
 * Created by khruzin-aa on 14.03.2018.
 */
public interface Executor extends Runnable {
    @Override
    void run();

    void setInformation(Information information);

    void setResult(Result result);
}
