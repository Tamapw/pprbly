package com.sbt.pprb.ac.loadtesting.executors.impl;

import com.sbt.pprb.ac.loadtesting.Result;
import com.sbt.pprb.ac.loadtesting.executors.Executor;
import com.sbt.pprb.ac.loadtesting.parsers.Information;
import com.sbt.pprb.ac.loadtesting.utils.ParserUtils;

import java.lang.reflect.Method;

public class JavaExecutorImpl implements Executor {

    Information information;
    Result result;

    @Override
    public void run() {
        Class aClass = ParserUtils.getClass(information.getClassAndMethodName());
        Object objectOfClass = ParserUtils.getObjectOfClass(aClass);
        Method method = ParserUtils.getMethod(information.getClassAndMethodName(), information.getArgs());

        long start = System.currentTimeMillis();
        try {
            Object invoke = method.invoke(objectOfClass, information.getArgs());
            result.incrementCountOfSuccess();
        } catch (Exception e) {
            result.incrementCountOfFailed();
        }
        result.getDurationsOfQuery().add(System.currentTimeMillis() - start);
    }

    @Override
    public void setInformation(Information information) {
           this.information = information;
    }

    @Override
    public void setResult(Result result) {
        this.result = result;
    }
}
