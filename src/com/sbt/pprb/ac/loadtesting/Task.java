package com.sbt.pprb.ac.loadtesting;

import com.sbt.pprb.ac.loadtesting.executors.Executor;
import com.sbt.pprb.ac.loadtesting.parsers.Information;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Task {
    private volatile Executor executor;
    private Information information;
    private Result result;
    private int time;
    private int countInSec;
    private int countOfRequest;
    private boolean isFinish = false;
    private boolean isCountOfRequest;

    private int countThreads;
    private int maxThreads = 100;
    private ThreadPoolExecutor threadPoolExecutor;

    private Task(int counter) {
        if (counter / 10 > maxThreads) {
            countThreads = maxThreads;
        } else {
            countThreads = counter;
        }

        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(countThreads);
    }

    public Task(Information information, int countOfRequest, Executor executor) {
        this(countOfRequest);
        this.information = information;
        this.executor = executor;
        this.countOfRequest = countOfRequest;
        isCountOfRequest = true;
    }

    public Task(Information information, int time, int countInSec, Executor executor) {
        this(countInSec);
        this.information = information;
        this.time = time;
        this.countInSec = countInSec;
        this.executor = executor;
        isCountOfRequest = false;
    }

    public void run() throws InterruptedException {
        result = new Result();
        executor.setInformation(information);
        executor.setResult(result);

        if (isCountOfRequest) {
            for (int i = 0; i < countOfRequest; i++) {
                threadPoolExecutor.execute(executor);
            }
        }
        threadPoolExecutor.shutdown();
        while (!threadPoolExecutor.isShutdown()) {
            Thread.sleep(1000);
        }

        isFinish = true;
    }

    public Result getResult() {
        return result;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public boolean isNotFinish() {
        return !isFinish;
    }
}
