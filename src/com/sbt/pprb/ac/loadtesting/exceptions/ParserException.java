package com.sbt.pprb.ac.loadtesting.exceptions;

public class ParserException extends RuntimeException {
    public ParserException(String error) {
        super(error);
    }

    public ParserException(String error, Exception innerException) {
        super(error, innerException);
    }

    public ParserException(Exception innerException) {
        super(innerException);
    }
}
