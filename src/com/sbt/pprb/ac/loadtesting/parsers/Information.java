package com.sbt.pprb.ac.loadtesting.parsers;

public class Information {
    private String classAndMethodName;
    private Object[] args;

    public Information(String classAndMethodName, Object... args) {
        this.classAndMethodName = classAndMethodName;
        this.args = args;
    }

    public String getClassAndMethodName() {
        return classAndMethodName;
    }

    public void setClassAndMethodName(String classAndMethodName) {
        this.classAndMethodName = classAndMethodName;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object... args) {
        this.args = args;
    }
}
