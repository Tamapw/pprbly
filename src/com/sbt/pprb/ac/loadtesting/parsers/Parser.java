package com.sbt.pprb.ac.loadtesting.parsers;

public interface Parser {
    Information parse(String str);
}
