package com.sbt.pprb.ac.loadtesting.expressions;

public abstract class Expression {
    protected Object value;

    public Expression(Object value) {
        this.value = value;
    }

    public abstract Object invoke();
}
