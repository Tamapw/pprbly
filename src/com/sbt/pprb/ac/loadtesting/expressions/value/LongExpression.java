package com.sbt.pprb.ac.loadtesting.expressions.value;

import com.sbt.pprb.ac.loadtesting.expressions.Expression;

public class LongExpression extends Expression {

    public LongExpression(Long value) {
        super(value);
    }

    @Override
    public Long invoke() {
        return (Long) value;
    }
}
