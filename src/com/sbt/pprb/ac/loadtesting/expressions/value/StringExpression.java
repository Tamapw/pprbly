package com.sbt.pprb.ac.loadtesting.expressions.value;

import com.sbt.pprb.ac.loadtesting.expressions.Expression;

public class StringExpression extends Expression {

    public StringExpression(String value) {
        super(value);
    }

    @Override
    public String invoke() {
        return (String) value;
    }
}
