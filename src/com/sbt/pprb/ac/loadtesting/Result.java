package com.sbt.pprb.ac.loadtesting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Result {
    private AtomicInteger countOfSuccess = new AtomicInteger(0);
    private AtomicInteger countOfFail = new AtomicInteger(0);
    private List<Long> durationsOfQuery = Collections.synchronizedList(new ArrayList<>());

    public AtomicInteger getCountOfSuccess() {
        return countOfSuccess;
    }

    public void setCountOfSuccess(AtomicInteger countOfSuccess) {
        this.countOfSuccess = countOfSuccess;
    }

    public AtomicInteger getCountOfFail() {
        return countOfFail;
    }

    public void setCountOfFail(AtomicInteger countOfFail) {
        this.countOfFail = countOfFail;
    }

    public List<Long> getDurationsOfQuery() {
        return durationsOfQuery;
    }

    public void incrementCountOfSuccess() {
        countOfSuccess.addAndGet(1);
    }

    public void incrementCountOfFailed() {
        countOfFail.addAndGet(1);
    }
}
