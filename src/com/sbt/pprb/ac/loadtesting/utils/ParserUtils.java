package com.sbt.pprb.ac.loadtesting.utils;

import com.sbt.pprb.ac.loadtesting.exceptions.ParserException;

import java.lang.reflect.Method;

public class ParserUtils {

    public static Class getClass(String classAndMethodName) {
        String[] strings = classAndMethodName.split("#");
        if (strings.length == 1) {
            throw new ParserException("Переданная строка не является отображением класса.");
        }

        String className = strings[0];
        Class resultClass;
        try {
            resultClass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new ParserException("Класс полученный из введённой строки не был найден", e);
        }

        return resultClass;
    }

    public static Method getMethod(String classAndMethodName, Object[] args) {
        Class[] classesOfArgs = new Class[args.length];
        for (int i = 0; i < args.length; i++) {
            classesOfArgs[i] = args[i].getClass();
        }

        Class<?> aClass = getClass(classAndMethodName);
        String[] strings = classAndMethodName.split("#");
        if (strings.length == 1) {
            throw new ParserException("Переданная строка не является отображением класса.");
        }
        String methodName = strings[1];

        Method method;
        try {
            method = aClass.getMethod(methodName, classesOfArgs);
        } catch (NoSuchMethodException e) {
            throw new ParserException("Класс полученный из введённой строки не был найден", e);
        }

        return method;
    }

    public static Object getObjectOfClass(Class<?> aClass) {
        Object obj;
        try {
            obj = aClass.newInstance();
        } catch (Exception e) {
            throw new ParserException(e);
        }

        return obj;
    }
}
