package com.sbt.pprb.ac.loadtesting;

import com.sbt.pprb.ac.loadtesting.executors.Executor;
import com.sbt.pprb.ac.loadtesting.parsers.Information;
import com.sbt.pprb.ac.loadtesting.parsers.Parser;
import com.sbt.pprb.ac.loadtesting.parsers.impl.JavaParserImpl;
import com.sbt.pprb.ac.ui.controller.Controller;
import javafx.scene.chart.XYChart;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by khruzin-aa on 14.03.2018.
 */
public class Tester {
    private ArrayList<Task> tasks;
    private Parser parser;
    private Controller controller;

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void executeTask(String method, Long count, Executor executor) throws InterruptedException, IOException {
        //ToDo реализовать выбор парсера в зависимости от экзекьютора
        Parser parser = new JavaParserImpl();
        Information information = parser.parse(method);

        Task task = new Task(information, Math.toIntExact(count), executor);
        task.run();

        while (task.isNotFinish()) {
            Thread.sleep(1000);
        }

        XYChart.Series<Integer, Long> series = new XYChart.Series<>();

        List<Long> q = task.getResult().getDurationsOfQuery();
        for (int i = 0; i < q.size(); i++) {
            series.getData().add(new XYChart.Data<>(i, q.get(i)));
        }

        controller.chart.getData().add(series);
        controller.out.setText(task.getResult().getCountOfSuccess().toString());
        controller.outFail.setText(task.getResult().getCountOfFail().toString());
    }


}
