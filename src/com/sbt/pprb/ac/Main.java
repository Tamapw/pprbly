package com.sbt.pprb.ac;

import com.sbt.pprb.ac.loadtesting.Tester;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public static Tester tester = new Tester();

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("ui/view/sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 570, 275));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
