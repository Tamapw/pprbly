package com.sbt.pprb.ac.ui.controller;

import com.sbt.pprb.ac.Main;
import com.sbt.pprb.ac.loadtesting.Task;
import com.sbt.pprb.ac.loadtesting.executors.Executor;
import com.sbt.pprb.ac.loadtesting.executors.impl.JavaExecutorImpl;
import com.sbt.pprb.ac.loadtesting.utils.ParserUtils;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.io.IOException;

public class Controller {
    public ChoiceBox cbSelectApi;
    public Button btnBeginTest;
    public Pane rootContent;
    public TextField tfTestingMethod;
    public TextField tfCountOfRequest;
    public LineChart chart;
    public Label out;
    public Label outFail;

    public void cbSelectApi_clicked(MouseEvent mouseEvent) {

    }

    public void btnBeginTest_clicked(MouseEvent mouseEvent) {
        String executorName = (String) cbSelectApi.getValue();

        Executor executor = null;
        switch (executorName) {
            case "Java" : {
                executor = new JavaExecutorImpl();
            } break;
            case "PPRB" : {
                executor = null;
            } break;
            case "HTTP" : {
                executor = null;
            }
            default: {
                throw new RuntimeException("Wow");
            }
        }

        try {
            Main.tester.setController(this);
            Main.tester.executeTask(tfTestingMethod.getText(), Long.valueOf(tfCountOfRequest.getText()), executor);
        } catch (InterruptedException | IOException e) {
        }
    }
}
